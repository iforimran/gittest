pipelineJob("pipleineJob") {
	description("testOne")
	
  definition {
    cps {
    script ( 
    """

ansiColor('xterm') {
    node('master') {

        withEnv([
        'reponame=gittest.git',
        'branchname=master'
        ]) 

        {

            properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10', numToKeepStr: '15')), disableConcurrentBuilds(), [\$class: 'RebuildSettings', autoRebuild: false, rebuildDisabled: false], pipelineTriggers([pollSCM('* * * * *')])])
            cleanWs deleteDirs: true
            checkout([\$class: 'GitSCM', branches: [[name: "*/\${branchname}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[url: "git@gitlab.com:iforimran/\${reponame}"]]])
        

            stage("echo") {
                sh '''
                    echo \$(pwd)
                    '''
                }

            
    
}"""
)   
 		
    }
  }
}
